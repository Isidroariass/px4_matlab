clear all;clc
parametros; % Script que da valor a la estructura global 'param'. Esta almacena parámetros como las ganancias de los controladores o el tiempo de simulación
modo_control(param.modoControl);  % Función que modifica el estado de los switches del modelo de simulink para desactivar algunos controladores y activar otros
addpath('optimizacion') % Carpeta en la que se encuentran algunas funciones de utilidad

% X = fmincon(FUN,X0,A,B,Aeq,Beq,LB,UB,NONLCON,OPTIONS)
%       -FUN: Función a optimizar. Como entrada tiene un solo vector de tamaño
%      el número de parámetros.  
%       -X0:  Vector de estados iniciales
%       -A,B,Aeq,Beq: Matrices para establecer los límites (no se utilizan)
%       -X0:  Vector de estados iniciales
%       -LB:  Límite inferior de los parámetros
%       -UB:  Límite superior de los parámetros
%       -NONLCON: Función que establece límites (no se utiliza)
%       -OPTIONS: estrutura de datos en la que se indican las opciones,
%      como por ejemplo el método. 

% options =optimoptions('fmincon','Display','iter','Algorithm','interior-point','StepTolerance',1); % Se pide que se imprima por pantalla cada vez que se encuentre un mínimo y se elige el método de optimización 'punto interior'. Este es un algoritmo de gran escala el cual se recomienda para utilizarlo a la hora de enfrentarse a un problema nuevo.
options =optimoptions('fmincon','Display','iter','Algorithm','interior-point'); % Se pide que se imprima por pantalla cada vez que se encuentre un mínimo y se elige el método de optimización 'punto interior'. Este es un algoritmo de gran escala el cual se recomienda para utilizarlo a la hora de enfrentarse a un problema nuevo.

x0=[0.6981 ,  0, 0]; % Parámetros iniciales 
lb=[0,0,0]; % Límite inferior de los parámetros
ub=[30,0,0]; % Límite superior de los parámetros


tic % Funciones tic y toc para evaluar tiempo de ejecución la función.

x=fmincon(@fun_op,x0,[],[],[],[],lb,ub,[],options)% Comienzo de la optimización

toc
