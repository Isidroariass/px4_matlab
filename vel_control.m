function out=vel_control(in)
   Vel=[in(1);in(2);in(3)];
   Vel_sp=[in(4);in(5);in(6)];
   Acc=[in(7);in(8);in(9)];
   e_int=[in(10);in(11);in(12)];    
   
   global param

   Kp=param.MPC_Z_VEL_P;
   Ki=param.MPC_Z_VEL_I;
   Kd=param.MPC_Z_VEL_D;
  
   %%% Controlador en el eje z %%%
   Fz=Kp*(Vel_sp(3)-Vel(3)) + Kd*(0 - Acc(3)) + Ki*e_int(3) - param.MPC_THR_HOVER;
   
   % Antiwindup
   if (abs(e_int(3)*Ki) > abs(param.MPC_THR_MAX) )   || ...
            ( Fz > 0 && e_int(3)<0 )  || ...
            ( Fz < -param.MPC_THR_MAX && e_int(3)>0 )  
       integrar(3)=0;
   else
       integrar(3)=Vel_sp(3)-Vel(3);
   end
   
   Fz=max(Fz,-param.MPC_THR_MAX);
   Fz=min(Fz,0);
  
   %%% Controlador en los ejes XY %%%
   Kp=param.MPC_XY_VEL_P;
   Ki=param.MPC_XY_VEL_I;
   Kd=param.MPC_XY_VEL_D;
   Fxy=Kp*(Vel_sp(1:2)-Vel(1:2)) + Kd*(0 - Acc(1:2)) + Ki*e_int(1:2);
   
   % Saturaciones
   Fxy_mod=norm(Fxy);
   saturacion1=sqrt(param.MPC_THR_MAX^2 - Fz^2);
   saturacion2=abs(Fz*tand(param.MPC_TILTMAX_AIR));
   saturacion_min=min(saturacion1,saturacion2);
   Fxy_sat=Fxy;
   if Fxy_mod>saturacion_min
       Fxy_sat=Fxy/Fxy_mod*saturacion_min;
   end
   
   % La variable 'integrar' será una salida que se integrará fuera de este
   % bloque y entrará como 'e_int`
   if param.MPC_XY_VEL_I>0
        integrar(1:2)=Vel_sp(1:2)-Vel(1:2)  -2/Kp*(Fxy-Fxy_sat);
   else
       integrar(1:2)=[0,0];
   end
 
   out=[Fxy_sat;Fz;integrar'];

end
