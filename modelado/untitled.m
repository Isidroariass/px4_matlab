clear all
comp(1).nombre="motor1";
comp(1).peso=0;
comp(1).pos=[0 0 0];
comp(1).I=[0 0 0];

comp(2).peso=0;
comp(2).nombre="motor2";
comp(2).pos=[0 0  0];
comp(2).I=[0 0 0];

comp(3).nombre="Autopiloto"
comp(3).pos=[ 0   -2.5   0 ]
comp(3).peso=120 
comp(3).I=[0 0 0];

comp(4).nombre="GNSS"
comp(4).pos=[-9   -14   0 ]
comp(4).peso= 48 
comp(4).I=[0 0 0];

comp(5).nombre="Receptor"
comp(5).pos=[  3   -11   0 ]
comp(5).peso= 17
comp(5).I=[0 0 0];

comp(6).nombre="Telemetría"
comp(6).pos=[  4   -14   0 ]
comp(6).peso= 24
comp(6).I=[0 0 0];

comp(7).nombre="Buzzer"
comp(7).pos=[ 9   -3   0    ]
comp(7).peso=5
comp(7).I=[0 0 0];

comp(8).nombre="Motor + Hélice"
comp(8).pos=[ 0   43   -30 ]
comp(8).peso=184 
comp(8).I=[0 0 0];

comp(9).nombre="Motor + Hélice"
comp(9).pos=[  0   -43   -30] 
comp(9).peso= 184
comp(9).I=[0 0 0];

comp(10).nombre="ESC"
comp(10).pos=[ 0   43   -7  ]
comp(10).peso=26
comp(10).I=[0 0 0];

comp(11).nombre="ESC"
comp(11).pos=[ 0   -43   -7  ]
comp(11).peso=26
comp(11).I=[0 0 0];

comp(12).nombre="Módulo de alimentación"
comp(12).pos=[  0   16   0      ] 
comp(12).peso=14
comp(12).I=[0 0 0];

comp(13).nombre="Servomotor"
comp(13).pos=[ 0   45   -17 ] 
comp(13).peso= 57 
comp(13).I=[0 0 0];

comp(14).nombre="Servomotor"
comp(14).pos=[ 0   -45   -17 ] 
comp(14).peso= 57
comp(14).I=[0 0 0];

comp(15).nombre="Regulador"
comp(15).pos=[ -5   4   0 ] 
comp(15).peso= 18
comp(15).I=[0 0 0];

comp(16).nombre="Aluminio 85cm"
comp(16).pos=[ 0   0   0 ] 
comp(16).peso=79
comp(16).I=[44,0,44];

comp(17).nombre="Aluminio 35cm"
comp(17).pos=[  0   20   0]  
comp(17).peso=32.6
comp(17).I=[0,3.3,3.3];

comp(18).nombre="Aluminio 35cm"
comp(18).pos=[  0   -20   0]  
comp(18).peso=32.6
comp(18).I=[0,3.3,3.3];

comp(19).nombre="Aluminio 39cm"
comp(19).pos=[  9   0   0 ] 
comp(19).peso=36.3
comp(19).I=[4.56,0,4.56];

comp(20).nombre="Aluminio 39cm"
comp(20).pos=[  -9   0   0 ] 
comp(20).peso=36.3
comp(20).I=[4.56,0,4.56];

comp(21).nombre="Aluminio 25cm"
comp(21).pos=[ 0   43   0 ] 
comp(21).peso=23.3
comp(21).I=[1.2 1.2 0];

comp(22).nombre="Aluminio 25cm"
comp(22).pos=[ 0   -43   0 ] 
comp(22).peso=23.3
comp(22).I=[1.2  1.2  0];

comp(23).nombre="Metacrilato 19x32cm" 
comp(23).pos=[  0   0   0 ] 
comp(23).peso=226
comp(23).I=[19.29,   6.8,   26.09];

comp(24).nombre="Articulación "
comp(24).pos=[ 0   43   -25]  
comp(24).peso= 148 
comp(24).I=[0 0 0];

comp(25).nombre="Articulación "
comp(25).pos=[  0   -43   -25 ]
comp(25).peso=148
comp(25).I=[0 0 0];


n=length(comp);
PesoTotal=0;
cm=[0 0 0];
for i=1:n
    PesoTotal=PesoTotal+comp(i).peso;
    cm=cm+comp(i).pos*comp(i).peso;%*comp(i).peso;
end
cm=cm/PesoTotal

x_bat=-PesoTotal/149*cm(1)
y_bat=-PesoTotal/149*cm(2)

comp(26).nombre="Bateria "
comp(26).pos=[  x_bat   y_bat   0 ]
comp(26).peso=149
comp(26).I=[0 0 0];

n=length(comp);
cm_t=(cm*PesoTotal + [x_bat, y_bat,0]*149)/(149+PesoTotal)
PesoTotal=PesoTotal+149;
Jcm=zeros(3,3);
for i=1:n
    pos_aux=comp(i).pos- cm_t;
    m=comp(i).peso/1000;
   Jcm=Jcm +  diag(comp(i).I)  +  m*(norm(pos_aux)^2 * eye(3) - outerProduct(pos_aux));
end

Jcm
% Batería 
%   -   -   0 
%  149 \\
