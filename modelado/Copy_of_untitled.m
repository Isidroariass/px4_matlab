clear all
comp(1).nombre="motor1";
comp(1).peso=0;
comp(1).pos=[0 0 0];

comp(2).peso=0;
comp(2).nombre="motor2";
comp(2).pos=[0 0  0];

comp(3).nombre="Autopiloto"
comp(3).pos=[ 0   -2.5   0 ]
comp(3).peso=0 

comp(4).nombre="GNSS"
comp(4).pos=[-9   -14   0 ]
comp(4).peso= 0

comp(5).nombre="Receptor"
comp(5).pos=[  3   -11   0 ]
comp(5).peso= 0

comp(6).nombre="Telemetría"
comp(6).pos=[  4   -14   0 ]
comp(6).peso= 0

comp(7).nombre="Buzzer"
comp(7).pos=[ 9   -3   0    ]
comp(7).peso=0

comp(8).nombre="Motor + Hélice"
comp(8).pos=[ 0   43   -30 ]
comp(8).peso=0

comp(9).nombre="Motor + Hélice"
comp(9).pos=[  0   -43   -30] 
comp(9).peso= 0

comp(10).nombre="ESC"
comp(10).pos=[ 0   43   -7  ]
comp(10).peso=0

comp(11).nombre="ESC"
comp(11).pos=[ 0   -43   -7  ]
comp(11).peso=0

comp(12).nombre="Módulo de alimentación"
comp(12).pos=[  0   16   0      ] 
comp(12).peso=0

comp(13).nombre="Servomotor"
comp(13).pos=[ 0   45   -17 ] 
comp(13).peso= 0 

comp(14).nombre="Servomotor"
comp(14).pos=[ 0   -45   -17 ] 
comp(14).peso=0

comp(15).nombre="Regulador"
comp(15).pos=[ -5   4   0 ] 
comp(15).peso= 0

comp(16).nombre="Aluminio 85cm"
comp(16).pos=[ 0   0   0 ] 
comp(16).peso=0

comp(17).nombre="Aluminio 35cm"
comp(17).pos=[  0   20   0]  
comp(17).peso=0

comp(18).nombre="Aluminio 35cm"
comp(18).pos=[  0   -20   0]  
comp(18).peso=0

comp(19).nombre="Aluminio 39cm"
comp(19).pos=[  9   0   0 ] 
comp(19).peso=0

comp(20).nombre="Aluminio 39cm"
comp(20).pos=[  -9   0   0 ] 
comp(20).peso=0

comp(21).nombre="Aluminio 25cm"
comp(21).pos=[ 0   43   0 ] 
comp(21).peso=0

comp(22).nombre="Aluminio 25cm"
comp(22).pos=[ 0   -43   0 ] 
comp(22).peso=0

comp(23).nombre="Metacrilato 19x32cm" 
comp(23).pos=[  0   0   0 ] 
comp(23).peso=0

comp(24).nombre="Articulación "
comp(24).pos=[ 0   43   -25]  
comp(24).peso= 0 

comp(25).nombre="Articulación "
comp(25).pos=[  0   -43   -25 ]
comp(25).peso=0


n=length(comp);
PesoTotal=0;
cm=[0 0 0];
for i=1:n
    PesoTotal=PesoTotal+comp(i).peso;
    cm=cm+comp(i).pos%*comp(i).peso;
end
cm=cm/PesoTotal

x_bat=PesoTotal/149*cm(1)
y_bat=PesoTotal/149*cm(2)


% Batería 
%   -   -   0 
%  149 \\
