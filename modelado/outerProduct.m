function out=outerProduct(R)
out=zeros(3,3);
for i=1:3
    for j=1:3
        out(i,j)=R(i)*R(j);
    end
end

end

