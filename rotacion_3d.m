function out= rotacion_3d(in)
   F_NED=[in(1);in(2);in(3)];
   yaw_sp=in(4);

   body_z=-F_NED/(norm(F_NED));
   y_c=[-sin(yaw_sp); cos(yaw_sp); 0];
   body_x= cross(y_c,body_z)/norm(cross(y_c,body_z));
   body_y= cross(body_z,body_x); 
   R=[body_x, body_y, body_z];
   Fz=norm(F_NED);
   global param
   Fz=min( param.max_Fz, Fz);
   Fz=max(-param.max_Fz, Fz);
   out=[rotm2quat(R)';Fz];
end
