close all
offset=offset-param.step_ref;
%% Comparación seguimiento omega
    % Adaptación de tiempos de gazebo para que coincidan con simulink
    t_att_gt_off=t_att_gt/1e6-offset;
    t_outputs_off=t_outputs/1e6-offset;

%     hold on
%     plot(omega,'b','LineWidth',2);
%     plot(t_att_gt_off, [rolld_gaz,pitchd_gaz,yawd_gaz],'r','LineWidth',2);
% %     legend('\Omega (simulink)', '\Omega_{sp} (simulink)', '\Omega (gazebo)', '\Omega_{sp} (gazebo)');
%     title('Velocidad angular ante par constante');
    
%     t_vel_gt_off=t_vel_gt/1e6-offset;
%     hold on
%     plot(Xd,'b','LineWidth',2);
%     plot(t_vel_gt_off, [vx_gt_gaz,vy_gt_gaz,vz_gt_gaz],'r','LineWidth',2);
%     title('Velocidad lineal ante fuerza constante')

    figure
    hold on
    plot(simout.omega1,'b','LineWidth',2);
    plot(t_outputs_off, (output0-1000)/1000*param.max_omega,'r','LineWidth',2);
    title('Comparación de salidas 0');
    legend('simulink','gazebo');
    saveas(gcf,'figuras/comparacion/output0.fig');
    
    figure
    hold on
    plot(simout.omega2,'b','LineWidth',2);
    plot(t_outputs_off, (output1-1000)/1000*param.max_omega,'r','LineWidth',2);
    title('Comparación de salidas 1');
    legend('simulink','gazebo');
    saveas(gcf,'figuras/comparacion/output1.fig');
    
    figure
    hold on
    plot(simout.alfa1,'b','LineWidth',2);
    plot(t_outputs_off, (output2-1000)/1000*pi -pi/2,'r','LineWidth',2);
    title('Comparación de \alpha_1');
    legend('simulink','gazebo');
    saveas(gcf,'figuras/comparacion/output2.fig');
    
if (param.modoControl==1)
    t_att_gt_off=t_att_gt/1e6-offset;
    figure
    hold on
    plot(simout.omega,'','LineWidth',2);
    plot(simout.omega_sp,'--');
    plot(t_att_gt_off, [rolld_gaz,pitchd_gaz,yawd_gaz],'','LineWidth',2);
    title('Comparación de la velocidad angular');
    saveas(gcf,'figuras/comparacion/omega.fig');
end
if (param.modoControl>1)
    % Adaptación de tiempos de gazebo para que coincidan con simulink
    t_rate_sp_off=t_rate_sp/1e6-offset;
    t_att_gt_off=t_att_gt/1e6-offset;
    t_att_off=t_att/1e6-offset;
    t_controls_off=t_controls/1e6-offset;
    
    
    figure
    hold on
    plot(simout.omega,'','LineWidth',2);
    plot(simout.omega_sp,'--');
    plot(t_att_gt_off, [rolld_gaz,pitchd_gaz,yawd_gaz],'','LineWidth',2);
    plot(t_rate_sp_off,[rolld_sp,pitchd_sp,yawd_sp],'--');
    plot(t_att_off, [rolld_est,pitchd_est,yawd_est],'','LineWidth',2);
    legend('\Omega (simulink)','\Omega (simulink)','\Omega (simulink)', '\Omega_{sp} (simulink)', '\Omega_{sp} (simulink)', '\Omega_{sp} (simulink)', '\Omega_{gt} (gazebo)', '\Omega_{gt} (gazebo)', '\Omega_{gt} (gazebo)', '\Omega_{sp} (gazebo)', '\Omega_{sp} (gazebo)', '\Omega_{sp} (gazebo)', '\Omega (gazebo)', '\Omega (gazebo)', '\Omega (gazebo)');
    title('Comparación de seguimiento a la velocidad angular');
    grid;
    estiliza_figura
    saveas(gcf,'figuras/comparacion/omega.fig');
    
    figure
    hold on
    plot(simout.Fz,'b','LineWidth',2);
    plot(t_controls_off, control_3,'r','LineWidth',2);
    legend('F_z (simulink)','F_z (gazebo)');
    title('Comparación de fuerzas deseada (F_z)');
    saveas(gcf,'figuras/comparacion/Fz.fig');
    
    figure
    hold on
    plot(simout.tau_des,'b','LineWidth',2);
    plot(t_controls_off, [control_0, control_1, control_2],'r','LineWidth',2);
    legend('\Omega (simulink)', '\Omega_{sp} (simulink)', '\Omega (gazebo)', '\Omega_{sp} (gazebo)');
    title('Comparación de pares deseados');
    saveas(gcf,'figuras/comparacion/tau.fig');
    

end
if (param.modoControl>2)
    t_att_off=t_att/1e6-offset;
    t_att_gt_off=t_att_gt/1e6-offset;
    t_att_ref_off=t_att_ref/1e6-offset;
    
    % Referencias de ángulo expresadas en ángulos de euler
    euler_sp=fliplr(quat2eul([simout.quat_sp.Data(:,1),simout.quat_sp.Data(:,2),simout.quat_sp.Data(:,3),simout.quat_sp.Data(:,4)]));
    
    figure
    hold on
    plot(simout.euler,'','LineWidth',2)
    plot(simout.quat_sp.Time,euler_sp,'--')
    plot(t_att_gt_off,[roll_gt_gaz,pitch_gt_gaz,yaw_gt_gaz],'','LineWidth',2);
    plot(t_att_ref_off,[roll_ref_gaz, pitch_ref_gaz, yaw_ref_gaz],'--');
    plot(t_att_off, [roll_gz,pitch_gz,yaw_gz],'','LineWidth',2)
    title('Comparación del seguimiento a ángulo');
    legend('\xi (simulink)','\xi (simulink)','\xi (simulink)', '\xi_{sp} (simulink)', '\xi_{sp} (simulink)', '\xi_{sp} (simulink)', '\xi_{gt} (gazebo)', '\xi_{gt} (gazebo)', '\xi_{gt} (gazebo)', '\xi_{sp} (gazebo)', '\xi_{sp} (gazebo)', '\xi_{sp} (gazebo)', '\xi (gazebo)', '\xi (gazebo)', '\xi (gazebo)');
    estiliza_figura;
    saveas(gcf,'figuras/comparacion/euler.fig');
   
end

if (param.modoControl>3)
    t_vel_ref_off=t_vel_ref/1e6-offset;
    figure;
    hold on
    plot(simout.F_NED,'','LineWidth',2);
    plot(t_vel_ref_off,[thr0_gaz,thr1_gaz,thr2_gaz],'','LineWidth',2)
    title('Comparación F_{NED}');
    grid;
    xlabel('t(s)')
    ylabel('F (N)')
    legend('x','y','z')
    saveas(gcf,'figuras/comparacion/F_NED.fig');
end

if (param.modoControl>4)
    t_vel_off=t_vel/1e6-offset;
    t_vel_gt_off=t_vel_gt/1e6-offset;
    t_vel_ref_off=t_vel_ref/1e6-offset;
    figure
    hold on
    plot(simout.Xd,'','LineWidth',2);
    plot(simout.Xd_sp,'--');
    plot(t_vel_gt_off, [vy_gt_gaz,vx_gt_gaz,vz_gt_gaz]'','LineWidth',2);
    plot(t_vel_ref_off,[vy_ref_gaz,vx_ref_gaz,vz_ref_gaz],'--');
    plot(t_vel_off,[vy_gaz,vx_gaz,vz_gaz],'','LineWidth',2);
    
    title('Comparación del seguimiento a velocidad');
    legend('V (simulink)','V (simulink)','V (simulink)', 'V_{sp} (simulink)', 'V_{sp} (simulink)', 'V_{sp} (simulink)', 'V_{gt} (gazebo)', 'V_{gt} (gazebo)', 'V_{gt} (gazebo)', 'V_{sp} (gazebo)', 'V_{sp} (gazebo)', 'V_{sp} (gazebo)', 'V (gazebo)', 'V (gazebo)', 'V (gazebo)');

    estiliza_figura;
    saveas(gcf,'figuras/comparacion/vel.fig');
   
end

if (param.modoControl>5)
    t_vel_off=t_vel/1e6-offset;
    t_vel_gt_off=t_vel_gt/1e6-offset;
    t_vel_ref_off=t_vel_ref/1e6-offset;
    figure
    hold on
    plot(simout.X,'','LineWidth',2);
    plot(simout.X_sp,'--');
    plot(t_vel_gt_off, [y_gt_gaz,x_gt_gaz,z_gt_gaz]'','LineWidth',2);
    plot(t_vel_ref_off,[y_ref_gaz,x_ref_gaz,z_ref_gaz],'--');
    plot(t_vel_off,[y_gaz,x_gaz,z_gaz],'','LineWidth',2);
    
    title('Comparación del seguimiento a posición');
    legend('P (simulink)','P (simulink)','P (simulink)', 'P_{sp} (simulink)', 'P_{sp} (simulink)', 'P_{sp} (simulink)', 'P_{gt} (gazebo)', 'P_{gt} (gazebo)', 'P_{gt} (gazebo)', 'P_{sp} (gazebo)', 'P_{sp} (gazebo)', 'P_{sp} (gazebo)', 'P (gazebo)', 'P (gazebo)', 'P (gazebo)');

    estiliza_figura;
    saveas(gcf,'figuras/comparacion/pos.fig');
   
end

return

%%


t_vel_off=t_vel/1e6-offset;
t_vel_gt_off=t_vel_gt/1e6-offset;
t_vel_ref_off=t_vel_ref/1e6-offset;

fig=figure;
modifica_figure
hold on
plot(t,xd,'b','LineWidth',2);
plot(t,xd_ref,'b--');
plot(t_vel_gt_off, vy_gt_gaz,'r','LineWidth',2);
plot(t_vel_ref_off, vy_ref_gaz,'r--');
legend('sk xd', 'sk_xd_ref', 'gazebo_xd', 'gazebo_xd_ref');
title('Comparación de seguimiento a velocidad');

%%
close all
offset=152.9+0.064;
offset=offset-1;
t_off=t_att_gt/1e6-offset;
t_ref_off=t_rate_sp/1e6-offset;


fig=figure;
modifica_figure
hold on
plot(t,thetad,'b','LineWidth',2);
plot(t,thetad_ref,'b--');
plot(t_off, rolld_gaz,'r','LineWidth',2);
plot(t_ref_off, rolld_sp,'r--');
legend({'$\dot{\Omega}$ simulink', '$\dot{\Omega}_{ref}$ simulink', '$\dot{\Omega}$ gazebo', '$\dot{\Omega}_{ref}$ gazebo'},'Interpreter','latex');
title('Comparación de seguimiento a velocidad');
xlabel('Tiempo (s)');
ylabel('Velocidad angular (rad/s)');

t_off=t_controls/1e6-offset;
fig=figure;
axes1 = axes(fig);
set(axes1,'FontSize',14);
hold on
plot(t,actuator_control_0_1,'b','LineWidth',2);
plot(t_off,control_0,'r','LineWidth',2);
xlim([0.5 1.5]);
ylim([-0.2 3.5]);
grid;
set(axes1,'FontSize',12);
legend('\tau_x simulink','\tau_x gazebo');
title('Comparación de las señales de actuación');
xlabel('Tiempo (s)');
ylabel('Par de referencia (Nm)');
saveas(fig,'figuras/comparacion/comp_actuacion.eps','epsc')
saveas(fig,'figuras/comparacion/comp_actuacion.fig')

