function out = omega2euld(in)

   roll=in(1);
   pitch=in(2);
   yaw=in(3);
   p=in(4);
   q=in(5);
   r=in(6);
   A=[1,    sin(roll)*tan(pitch),   cos(roll)*tan(pitch);
      0,    cos(roll),              -sin(roll);
      0,    sin(roll)/cos(pitch),   cos(roll)/cos(pitch)];
   out=A*[p;q;r];
end

