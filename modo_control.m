function modo_control(modoControl)
    load_system('tiltrotor_sk.slx')
    % -  0 -> Ningún control. Se aplica señales de actuación constantes
    % -  1 -> Ningún control. Se aplica fuerzas y pares deseados
    % -  2 -> Control de velocidad angular
    % -  3 -> Control de orientación 
    % -  4 -> Control de Fuerzas expresadas en ejes inerciales (F_NED)
    % -  5 -> Control de velocidad
    % -  6 -> Control de posición

    %%% Se inicializán todos los interruptores como operación en bucle abierto
    set_param('tiltrotor_sk/Manual Switch Actuador1','sw','1');
    set_param('tiltrotor_sk/Manual Switch Actuador2','sw','1');
    set_param('tiltrotor_sk/Manual Switch Actuador3','sw','1');
    set_param('tiltrotor_sk/Manual Switch Actuador4','sw','1');
    set_param('tiltrotor_sk/Manual Switch Fuerza z','sw','1');
    set_param('tiltrotor_sk/Manual Switch Pares','sw','1');
    set_param('tiltrotor_sk/Manual Switch Rates','sw','1');
    set_param('tiltrotor_sk/Manual Switch q','sw','1');
    set_param('tiltrotor_sk/Manual Switch F_NED','sw','1');
    set_param('tiltrotor_sk/Manual Switch Xd','sw','1');

    %%% En función de la variable modoControl se activan ciertos controladores
    if modoControl > 0
        set_param('tiltrotor_sk/Manual Switch Actuador1','sw','0');
        set_param('tiltrotor_sk/Manual Switch Actuador2','sw','0');
        set_param('tiltrotor_sk/Manual Switch Actuador3','sw','0');
        set_param('tiltrotor_sk/Manual Switch Actuador4','sw','0');
    end
    if modoControl > 1 
%         set_param('test_3d/Manual Switch Fuerza z','sw','0');
        set_param('tiltrotor_sk/Manual Switch Pares','sw','0');
    end
    if modoControl > 2
        set_param('tiltrotor_sk/Manual Switch Rates','sw','0');
    end
    if modoControl > 3
        set_param('tiltrotor_sk/Manual Switch q','sw','0');
        set_param('tiltrotor_sk/Manual Switch Fuerza z','sw','0');
    end
    if modoControl > 4
        set_param('tiltrotor_sk/Manual Switch F_NED','sw','0'); 
    end
    if modoControl > 5
        set_param('tiltrotor_sk/Manual Switch Xd','sw','0');
    end
end
