clear all
addpath('../');
parametros;
s=tf('s');
Jx=param.Jx;
tau=param.tau_omega;
G=1/(Jx*s*(tau*s+1));
Kp=0.0740220330081702;
Kd=-0.0205752220392306;
Gbc=Kp*G/(1+Kp*G+Kd*G*s)
roots([Jx*tau, Jx+Kd, Kp])