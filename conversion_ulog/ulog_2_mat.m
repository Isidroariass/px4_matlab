%clear
% ulgFolder='2019-04-29';
% ulgFileName='14_15_07';
[stado,busqueda]=system("/home/isidro/buscador_nuevo/busca_nuevo");
aux=split(busqueda,'/');
ulgFolder=aux{1};
aux=aux{2};
aux=split(aux,'.');
ulgFileName=aux{1};
ruta=['/home/isidro/src/Firmware/build/px4_sitl_default/logs/' ulgFolder '/'];
% ruta=['/home/isidro/src/Firmware/build/px4_sitl_default/logs/' busqueda];
% delete(['*' ulgFileName '*.csv'])
delete(['*.csv'])
command = ['!ulog2csv ' '-o ./ ' ruta ulgFileName '.ulg'];
% command = ['!ulog2csv ' '-o ./ ' ruta];
try
	eval(command);
catch
	error('ERROR: failed to run ulog2csv, try: pip install pyulog');
end

vehicle_attitude_0=readtable([ulgFileName '_vehicle_attitude_0.csv']);
t_att=vehicle_attitude_0.timestamp;
q=[vehicle_attitude_0.q_0_, vehicle_attitude_0.q_1_, vehicle_attitude_0.q_2_, vehicle_attitude_0.q_3_];
eul=quat2eul(q);
roll_gz=eul(:,3);
pitch_gz=eul(:,2);
yaw_gz=eul(:,1);
rolld_est=vehicle_attitude_0.rollspeed;
pitchd_est=vehicle_attitude_0.pitchspeed;
yawd_est=vehicle_attitude_0.yawspeed;

vehicle_attitude_groundtruth_0=readtable([ulgFileName '_vehicle_attitude_groundtruth_0.csv']);
t_att_gt=vehicle_attitude_groundtruth_0.timestamp;
q=[vehicle_attitude_groundtruth_0.q_0_, vehicle_attitude_groundtruth_0.q_1_, vehicle_attitude_groundtruth_0.q_2_, vehicle_attitude_groundtruth_0.q_3_];
eul=quat2eul(q);
roll_gt_gaz=eul(:,3);
pitch_gt_gaz=eul(:,2);
yaw_gt_gaz=eul(:,1);

rolld_gaz=vehicle_attitude_groundtruth_0.rollspeed;
pitchd_gaz=vehicle_attitude_groundtruth_0.pitchspeed;
yawd_gaz=vehicle_attitude_groundtruth_0.yawspeed;

vehicle_rates_setpoint=readtable([ulgFileName '_vehicle_rates_setpoint_0.csv']);
rolld_sp=vehicle_rates_setpoint.roll;
pitchd_sp=vehicle_rates_setpoint.pitch;
yawd_sp=vehicle_rates_setpoint.yaw;
t_rate_sp=vehicle_rates_setpoint.timestamp;


vehicle_attitude_setpoint_0=readtable([ulgFileName '_vehicle_attitude_setpoint_0.csv']);
t_att_ref=vehicle_attitude_setpoint_0.timestamp;
roll_ref_gaz=vehicle_attitude_setpoint_0.roll_body;
pitch_ref_gaz=vehicle_attitude_setpoint_0.pitch_body;
yaw_ref_gaz=vehicle_attitude_setpoint_0.yaw_body;



vehicle_local_position_0=readtable([ulgFileName '_vehicle_local_position_0.csv']);
t_vel=vehicle_local_position_0.timestamp;
vy_gaz=vehicle_local_position_0.vy;
vx_gaz=vehicle_local_position_0.vx;
vz_gaz=vehicle_local_position_0.vz;
x_gaz=vehicle_local_position_0.x;
y_gaz=vehicle_local_position_0.y;
z_gaz=vehicle_local_position_0.z;

vehicle_local_position_groundtruth_0=readtable([ulgFileName '_vehicle_local_position_groundtruth_0.csv']);
t_vel_gt=vehicle_local_position_groundtruth_0.timestamp;
vy_gt_gaz=vehicle_local_position_groundtruth_0.vy;
vx_gt_gaz=vehicle_local_position_groundtruth_0.vx;
vz_gt_gaz=vehicle_local_position_groundtruth_0.vz;
x_gt_gaz=vehicle_local_position_groundtruth_0.x;
y_gt_gaz=vehicle_local_position_groundtruth_0.y;
z_gt_gaz=vehicle_local_position_groundtruth_0.z;

if param.modoControl>3
    vehicle_local_position_setpoint_0=readtable([ulgFileName '_vehicle_local_position_setpoint_0.csv']);
    t_vel_ref=vehicle_local_position_setpoint_0.timestamp;
    vy_ref_gaz=vehicle_local_position_setpoint_0.vy;
    vx_ref_gaz=vehicle_local_position_setpoint_0.vx;
    vz_ref_gaz=vehicle_local_position_setpoint_0.vz;
    x_ref_gaz=vehicle_local_position_setpoint_0.x;
    y_ref_gaz=vehicle_local_position_setpoint_0.y;
    z_ref_gaz=vehicle_local_position_setpoint_0.z;
    thr0_gaz=vehicle_local_position_setpoint_0.thrust_0_;
    thr1_gaz=vehicle_local_position_setpoint_0.thrust_1_;
    thr2_gaz=vehicle_local_position_setpoint_0.thrust_2_;
end



actuator_controls=readtable([ulgFileName '_actuator_controls_0_0.csv']);
control_0=actuator_controls.control_0_;
control_1=actuator_controls.control_1_;
control_2=actuator_controls.control_2_;
control_3=actuator_controls.control_3_;
t_controls=actuator_controls.timestamp;

actuator_outputs=readtable([ulgFileName '_actuator_outputs_0.csv']);
output0=actuator_outputs.output_0_;
output1=actuator_outputs.output_1_;
output2=actuator_outputs.output_2_;
output3=actuator_outputs.output_3_;
t_outputs=actuator_outputs.timestamp;

delete(['*.csv'])


