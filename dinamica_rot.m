function out=dinamica_rot(in)
   tau_b=[in(1); in(2); in(3)];
   omega=[in(4); in(5); in(6)];
   p=omega(1);
   q=omega(2);
   r=omega(3);

   global param
   Jx=param.Jx;
   Jy=param.Jy;
   Jz=param.Jz;
   omegad= tau_b./[param.Jx; param.Jy; param.Jz] + [(Jy-Jz)/Jx*q*r;
   (Jz-Jx)/Jy*p*r;   (Jx-Jy)/Jz*p*q];

   out=omegad;

end
