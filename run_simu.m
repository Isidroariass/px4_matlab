clear all
if(~exist('./figuras','file'))
    error('No se encuentra en la carpeta tilt-rotor-matlab')
end
%% Parametros
parametros;

%% Simulacion
modo_control(param.modoControl); % modificar el modelo de simulink activando unos bucles u otros
modelName = 'tiltrotor_sk';
load_system(modelName)
set_param(modelName,'Profile','off')
%open_system(modelName)
simout=sim(modelName);

%% Graficas
close all
dibuja_graficas;
close all;
return

%% Gazebo
addpath('conversion_ulog')
ulog_2_mat;

%% Calculo del offset temporal
t_rate_sp_off=t_rate_sp/1e6;
plot(t_rate_sp_off,[rolld_sp,pitchd_sp,yawd_sp],'--');

% figure;hold on
% plot(t_att_ref/1e6,roll_ref_gaz);
% plot(t_att_gt/1e6,roll_gt_gaz);
% plot(t_att/1e6,roll_gz)
% plot(t_vel_ref/1e6, vx_ref_gaz);
% t_outputs_off=t_outputs/1e6;
% plot(t_outputs_off, output0);

%% Graficas de comparación
offset=22.052 % segundos que hay que desplazar los logs de gazebo para que el escalón coincida a la vez que simulink
delete(['figuras/comparacion/*.fig'])
comparacion_sk_gazebo
close all
return

%% Estimación de la velocidad lineal
figure;
plot(t_vel_gt,[vx_gt_gaz,vy_gt_gaz,vz_gt_gaz]);hold on
plot(t_vel,[vx_gaz,vy_gaz,vz_gaz]);

%% Estimación de la velocidad angular
figure;
plot(t_att_gt/1e6, [rolld_gaz,pitchd_gaz,yawd_gaz],'','LineWidth',2);hold on
plot(t_att/1e6, [rolld_est,pitchd_est,yawd_est],'','LineWidth',2);

%% Estimación de la orientación 
figure
hold on
plot(t_att_gt/1e6,[roll_gt_gaz,pitch_gt_gaz,yaw_gt_gaz]);
plot(t_att/1e6,[roll_gz, pitch_gz, yaw_gz]);