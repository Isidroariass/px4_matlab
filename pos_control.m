function out = pos_control(in)
   
   x_sp=[in(1);in(2);in(3)];
   x=[in(4);in(5);in(6)];

   global param
   
   xd_sp=[param.MPC_XY_P;  param.MPC_XY_P; param.MPC_Z_P].*(x_sp-x);

   % Saturación 
   VELxy=[xd_sp(1); xd_sp(2)];
   velxy=norm(VELxy);
   if velxy > param.MPC_XY_VEL_MAX
       VELxy=VELxy/velxy*param.MPC_XY_VEL_MAX;
   end
   
   out=[VELxy; xd_sp(3)];
end
