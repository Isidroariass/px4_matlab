delete(['figuras/*.eps']);delete(['figuras/*.fig'])

fig=figure;
hold on
plot(simout.alfa1);
plot(simout.alfa2);
title('Inclinación de los rotores');
grid;
xlabel('t(s)')
ylabel('\alpha (rad)')
estiliza_figura;
legend('\alpha_1','\alpha_2');
saveas(fig,'figuras/alfa.fig');

fig=figure;
hold on
plot(simout.omega1);
plot(simout.omega2);
title('Velocidad de los rotores');
grid;
xlabel('t(s)')
ylabel('\omega (rad/s)')
estiliza_figura;
legend('\omega_1','\omega_2');
saveas(fig,'figuras/omega_rotores.fig');
    
%%% En función de la variable modoControl se activan ciertos controladores
if param.modoControl == 1 % Nigún controlador. Se suministran pares y fuerzas deseados
    fig=figure;
    hold on
    plot(simout.Xd);
    title('Xd');
    grid;
    saveas(fig,'figuras/xd.fig');
        
    fig=figure;
    hold on
    plot(simout.omega);
    title('omega');
    grid;
    saveas(fig,'figuras/omega.fig');

end
if param.modoControl > 1
    fig=figure;
    hold on
    plot(simout.omega);
    set(gca,'ColorOrderIndex',1)
    plot(simout.omega_sp,'--');
    legend('p','q','r')
    title('Control de \Omega');
    grid;
    xlabel('t(s)')
    ylabel('\Omega (rad/s)')
    estiliza_figura;
   
    saveas(fig,'figuras/omega.fig');
    saveas(fig,'figuras/omega.eps','epsc');
    
    fig=figure;
    hold on
    plot(simout.tau_des);
    legend('x','y','z')
    title('Pares deseados');
    grid;
    xlabel('t(s)')
    ylabel('\tau (N/m)')
    estiliza_figura;
   
    saveas(fig,'figuras/tau.fig');
    saveas(fig,'figuras/tau.eps','epsc');
    
end
if param.modoControl > 2
    fig=figure;
    hold on
    plot(simout.quat);
    plot(simout.quat_sp,'--');
    title('quat');
    grid;
    xlabel('t(s)')
    estiliza_figura;
    saveas(fig,'figuras/quat.fig');
    saveas(fig,'figuras/quat.eps','epsc');
    
    fig=figure;
    hold on
    plot(simout.quat.Time, fliplr(quat2eul([simout.quat.Data(:,1),simout.quat.Data(:,2),simout.quat.Data(:,3),simout.quat.Data(:,4)])));
%     set(gca, 'ColorOrder', circshift(get(gca, 'ColorOrder'), numel(fig)))
    set(gca,'ColorOrderIndex',1)
    plot(simout.quat_sp.Time, fliplr(quat2eul([simout.quat_sp.Data(:,1),simout.quat_sp.Data(:,2),simout.quat_sp.Data(:,3),simout.quat_sp.Data(:,4)])),'--');
    title('Ángulos de euler');
    xlabel('t(s)')
    ylabel('\eta (rad)')
    estiliza_figura;
    grid;
    legend('roll','pitch','yaw')
    saveas(gcf,'figuras/eul.fig');
    saveas(gcf,'figuras/eul.eps','epsc');
end
if param.modoControl > 3
    fig=figure;
    hold on
    plot(simout.F_NED);
    title('Fuerzas deseadas en ejes inerciales');
    grid;
    xlabel('t(s)')
    ylabel('F_{NED} (N)')
    estiliza_figura;
    legend('x','y','z')
    saveas(fig,'figuras/F_NED.fig');
    saveas(fig,'figuras/F_NED.eps','epsc');
    
    fig=figure;
    hold on
    plot(simout.Xdd);
    title('Xdd');
    grid;
    xlabel('t(s)')
    estiliza_figura;
    legend('x','y','z')
    saveas(fig,'figuras/Xdd.fig');
    saveas(fig,'figuras/Xdd.eps','epsc');
end
if param.modoControl > 4
    fig=figure;
    hold on
    plot(simout.Xd.Time,simout.Xd.Data(:,1));
    plot(simout.Xd.Time,simout.Xd.Data(:,2));
    plot(simout.Xd.Time,simout.Xd.Data(:,3));
    set(gca,'ColorOrderIndex',1)
    plot(simout.Xd_sp.Time,simout.Xd_sp.Data(:,1),'--');
    plot(simout.Xd_sp.Time,simout.Xd_sp.Data(:,2),'--');
    plot(simout.Xd_sp.Time,simout.Xd_sp.Data(:,3),'--');
    legend('x','y','z')
    title('Control de velocidad');
    grid;
    xlabel('t (s)')
    ylabel('V (m/s)')
    estiliza_figura;
    saveas(fig,'figuras/xd.fig');
    saveas(fig,'figuras/xd.eps','epsc');
end
if param.modoControl > 5
    fig=figure;
    hold on
    plot(simout.X.Time,simout.X.Data(:,1));
    plot(simout.X.Time,simout.X.Data(:,2));
    plot(simout.X.Time,simout.X.Data(:,3));
    set(gca,'ColorOrderIndex',1)
    plot(simout.X_sp.Time,simout.X_sp.Data(:,1),'--');
    plot(simout.X_sp.Time,simout.X_sp.Data(:,2),'--');
    plot(simout.X_sp.Time,simout.X_sp.Data(:,3),'--');
    title('Control de posición');
    grid;
    xlabel('t(s)')
    ylabel('P (m)')
    estiliza_figura;
    legend('x','y','z')
    saveas(fig,'figuras/x.fig');
    saveas(fig,'figuras/x.eps','epsc');
end

