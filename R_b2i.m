function out= R_b2i(in)
 F=[in(1); in(2); in(3)];
 eul=[in(4); in(5); in(6)];


 roll=eul(1);
 pitch=eul(2);
 yaw=eul(3);

 R=[cos(pitch)*cos(yaw), sin(roll)*sin(pitch)*cos(yaw)-cos(roll)*sin(yaw), cos(roll)*sin(pitch)*cos(yaw)+sin(roll)*sin(yaw);
    cos(pitch)*sin(yaw), sin(roll)*sin(pitch)*sin(yaw)+cos(roll)*cos(yaw), cos(roll)*sin(pitch)*sin(yaw)-sin(roll)*cos(yaw);
    -sin(pitch)         ,sin(roll)*cos(pitch)                            ,cos(roll)*cos(pitch)                           ];

 out=R*F;
end
