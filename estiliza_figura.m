function estiliza_figura()
    lines = findobj(gcf,'Type','Line');
    for i = 1:numel(lines)
      lines(i).LineWidth = 2.0;
    end
    
    ejes = findobj(gcf,'Type','axes');
    ejes.FontSize=14;
end