function out= mixer_3d(in)
   taux=in(1);
   tauy=in(2);
   tauz=in(3);
   Fz=in(4);
   global param
   
   ly=param.ly;
   lz=param.lz;
   
   T2= taux/(2*ly) + Fz/2;
   T1=-taux/(2*ly) + Fz/2;
   if T1<0
        T1=0;
   end
   if T2<0
        T2=0;
   end

   if T1<0.01
       alfa1=0;
   else
       alfa1=1/(2*lz*T1)*tauy  + 1/(2*ly*T1)*tauz;
       alfa1=min(max(alfa1,-param.max_alfa),param.max_alfa);
   end
   

   
   if T2<0.01
       alfa2=0;
   else
       alfa2=1/(2*lz*T2)*tauy  - 1/(2*ly*T2)*tauz;
       alfa2=min(max(alfa2,-param.max_alfa),param.max_alfa);
   end
    
   omega1=sqrt(T1/param.cT);
   omega2=sqrt(T2/param.cT);
   
   out=[omega1;omega2;alfa1;alfa2];
   if taux>0.1
   end
end
