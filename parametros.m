global param

%%% Modelo tiltrotor %%%
param.m=1; % Masa total del tiltrotor [Kg]
param.lz=0.25; % Distancia en el eje z desde el c.m. hasta los rotores 
param.ly=0.5; % Distancia en el eje y desde el c.m. hasta los rotores 
param.Jx=0.03; % Inercia en el eje x [Kg.m^2]
param.Jy=0.04; % Inercia en el eje y [Kg.m^2]
param.Jz=0.09; % Inercia en el eje z [Kg.m^2]
param.g=9.8; % Aceleración de la gravedad [m/s²]
param.cT=8.0992e-06; % Coeficiente de empuje de los rotores 
param.cm=0; % Coeficiente de arrastre de los rotores 
param.max_omega=1100; % Máxima velocidad de los rotores [rad/s]
param.max_alfa=pi/3; % Máxima inclinación de lor rorores [rad]
param.max_taux=30; % Máximo par en el eje x que genera control de velocidad angular [N.m]
param.max_tauy=30; % Máximo par en el eje y que genera control de velocidad angular [N.m]
param.max_tauz=30; % Máximo par en el eje z que genera control de velocidad angular [N.m]
param.max_Fz=2*(param.max_omega)^2*param.cT; % Máxima fuerza en el eje z del cuerpo  [N]
%param.max_T=2*(max_omega)^2*cT; %Hay dos rotores
param.tau_omega=0.01; % Constante de tiempo de los motores que hacen girar los rotores [s]
param.tau_alfa=0.01; % Constante de tiempo de los motores que inclinan el conjunto motor-rotor [s]

%%% Opciones de la simulación %%%
param.modoControl=6; % Esta variable indica cuales de los controladores están activos. 0- Ningún controlador; 1 - Control Mixer; 2 - Control de velocidad angular; 3 - Control de ángulo; 4 - Control de Fuerzas en ejes inerciales; 5 - Control de velocidad; 6 . Control de posición. 
param.Tm=4e-3; % Tiempo de discretización de algunos controladores [s]
param.tsim_run_simu=8; % tiempo de simulación [s]. 
param.step_ref=0.1; % Instante de tiempo en el que se aplica el escalon a la referencia [s]

%%% Referencias %%%
param.omega1_ref=1100; % Velocidad de giro del rotor 1 [rad/s]. Tiene efecto cuando modoControl=0.
param.omega2_ref=1100; % Velocidad de giro del rotor 2 [rad/s]. Tiene efecto cuando modoControl=0.
param.alfa1_ref=0; % Ángulo de inclinación del motor 1 [rad]. Tiene efecto cuando modoControl=0.
param.alfa2_ref=0; % Ángulo de inclinación del motor 1 [rad]. Tiene efecto cuando modoControl=0.
param.tau_ref=[0;0;0]; % Referencias de par  en los tres ejes [N.m] (modoControl=1).
param.Fz_ref=param.g*param.m; % Fuerza en el eje z del vehículo [N] (modoControl=1 || modoControl=2).
param.omega_ref=[0;0;0]; % Referencias de velocidades angulares en los tres ejes [rad/s] (modoControl=2).
param.eul_ref=[0,0,0]; % Referencias de orientación expresadas en ángulor de euler en el orden roll, pitch y yaw [rad] (modo_control=3).
param.FNED_ref=[param.g*param.m/4;0;-param.g*param.m]; % Referencias de fuerza expresada en ejes inerciales [N] (modoContros=4).
param.yaw_sp=0; % Ángulo yaw deseado [rad] (modoControl=4).
param.v_ref=[0;0;0]; % Velocidad de referencia [m/s] (modoControl=5).
param.p_ref=[0;0;0]; % Posición de referencia [m] (modoControl=6).
param.eje_exp=1; % indica en qué eje del vehículo  se hará el experimento: 0->eje x; 1->eje y; 2->eje z
amp_omega=3.5; % Amplitud del escalón en la referencia de velocidad angular [rad/s]. Será aplicado en un eje u otro dependiendo del valor de 'eje_exp'
amp_v=-2; % Amplitud del escalón en la referencia de velocidad [m/s]
amp_p=4; % Amplitud del escalón en la referencia de posición [m]
amp_tau=2.5; % Amplitud del escalón en la referencia de  [m]
amp_eul=-0.785; % Amplitud del escalón en la referencia de posición [m]
switch param.eje_exp
    case 0
        % Eje x
        param.omega_ref(1)=amp_omega;
        param.tau_ref(1)=amp_tau;
        param.eul_ref(1)=amp_eul;
        param.v_ref(2)=amp_v; % Si se está evaluando el control de rotación sobre el eje x, habrá que generar un escalón de velocidad en el eje y
        param.p_ref(2)=amp_omega;
    case 1 
        % Eje y
        param.omega_ref(2)=amp_omega;
        param.tau_ref(2)=amp_tau;
        param.eul_ref(2)=amp_eul;
        param.v_ref(1)=amp_v;
        param.p_ref(1)=amp_p;
    case 2
        % Eje z
        param.omega_ref(3)=amp_omega;
        param.tau_ref(3)=amp_tau;
        param.eul_ref(3)=amp_eul;
        param.v_ref(3)=amp_v;
        param.p_ref(3)=amp_p;
end
param.q_ref=eul2quat(fliplr(param.eul_ref)); % Referencia de orientación expresada en cuaternios. La función  'eul2quat' recibe los ángulos de euler en orden yaw, pitch y yaw, por esa razón  se utiliza la otra función 'fliplr' para invertir el orden del vector.

%%% Perturbaciones %%%
param.F_pert=[0;0;0]; % Fuerza de perturbación en ejes inerciales [N].
param.tau_pert=[0;0;0]; % Pares de perturbación en ejes cuerpo [N.m]
amp_tau_pert= 0.5; % N.m 
amp_F_pert=0.2; % N
if param.modoControl==2 || param.modoControl==3 
    % Solo se aplicará una perturbación de par cuando se estén evaluando el controlador de velocidad angular o el de ángulo
    switch param.eje_exp
    case 0
        param.tau_pert(1)=amp_tau_pert; 
    case 1 
        param.tau_pert(2)=amp_tau_pert;
    case 2
        param.tau_pert(3)=amp_tau_pert;
    end
end
if param.modoControl==5 || param.modoControl==6 
    % Solo se aplicará una perturbación de fuerza cuando se estén evaluando o el controlador de velocidad o el de posición. 
    switch param.eje_exp
    case 0
        param.F_pert(2)=amp_F_pert; 
    case 1 
        param.F_pert(1)=amp_F_pert;
    case 2
        param.F_pert(3)=amp_F_pert;
    end
end
param.delay_pert=param.tsim_run_simu/2; % Instante en el que se aplica la perturbación mantenida [s].

%%% Controlador de posición %%%
param.MPC_XY_P=0.6981; 
param.MPC_Z_P=0.5;

%%% Controlador de velocidad %%%
param.MPC_Z_VEL_P=39.4784;
param.MPC_Z_VEL_I=0;
param.MPC_Z_VEL_D=0.2566;
% Analítico
param.MPC_XY_VEL_P=  2.9588 ;
param.MPC_XY_VEL_I=0;
param.MPC_XY_VEL_D=  0.0472;
% optimización 
% param.MPC_XY_VEL_P= 7.3701;
% param.MPC_XY_VEL_I= 0.4456;
% param.MPC_XY_VEL_D= 0.1202;
param.MPC_THR_HOVER=param.g*param.m;
param.MPC_THR_MAX=param.g*param.m*2;
param.MPC_THR_MIN=0;
param.MPC_TILTMAX_AIR=45; %grados
param.MPC_XY_VEL_MAX=12;

%%% Controlador de orientación  %%%
param.MC_ROLL_P=12.57;
param.MC_PITCH_P=10.4720; % analítico
% param.MC_PITCH_P=12.4518; %optimización
param.MC_YAW_P=12.57;
param.MC_ROLLRATE_MAX=200;
param.MC_PITCHRATE_MAX=360;
param.MC_YAWRATE_MAX=200;

%%% Controlador de velocidad angular %%%
% Diseño analitico
param.MC_ROLLRATE_P=0;
param.MC_ROLLRATE_I=0;
param.MC_ROLLRATE_D=0;
% analítico 
param.MC_PITCHRATE_P=2.4674;
param.MC_PITCHRATE_I=0;
param.MC_PITCHRATE_D=0.0228;
% optimización
% param.MC_PITCHRATE_P=4.2473;
% param.MC_PITCHRATE_I=22.5534;
% param.MC_PITCHRATE_D=0.0343;
param.MC_YAWRATE_P=0;
param.MC_YAWRATE_I=0;
param.MC_YAWRATE_D=0;
param.MC_RR_INT_LIM=1;
param.MC_PR_INT_LIM=0.5;
param.MC_YR_INT_LIM=1;


%%% Filtros %%%
% Coeficientes del filtro para estimar la aceleración angular
param.MC_DTERM_CUTOFF=30;
K=tan(2*pi*30*4e-3/2);
DE=K^2 + sqrt(2)*K +1;
param.b0=K^2/DE;
param.b1=2*K^2/DE;
param.b2=K^2/DE;
param.a1=2*(K^2 -1)/DE;
param.a2=(1-K*sqrt(2) + K^2)/DE;
% Filtro utilizado para estimar la aceleración lineal
% param.MPC_VELD_LP=5;
 param.MPC_VELD_LP=5;
b=2*pi*param.MPC_VELD_LP*param.Tm*2;
param.a=b/(1+b);

clearvars -except param
