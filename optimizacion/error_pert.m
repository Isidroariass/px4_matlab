function out = error_pert(in)
    global Kp
    global Ki
    global Kd
    Jx=0.03;
    tau_m=0.15;
    y=in(1);
    yd=in(2);
    ydd=in(3);
    yddd=1/(tau_m*Jx)*(1 - y*Ki - yd*Kp - (Jx+Kd)*ydd);
    out=yddd;
end

