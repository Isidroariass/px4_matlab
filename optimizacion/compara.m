function compara(x1,x2,x3,p1,p2,p3)

    tsim=20;
    Kp1=x1(1);
    Kd1=x1(2);
    Ki1=x1(3);
    
    Kp2=x2(1);
    Kd2=x2(2);
    Ki2=x2(3);
    
    Kp3=x3(1);
    Kd3=x3(2);
    Ki3=x3(3);
    
    [y1,u1,r1,t1] = sim_seg(Kp1,Ki1,Kd1,tsim);
    [y2,u2,r2,t2] = sim_seg(Kp2,Ki2,Kd2,tsim);
    [y3,u3,r3,t3] = sim_seg(Kp3,Ki3,Kd3,tsim);

    fig=figure;hold on;
    plot(t1,y1);
    plot(t2,y2);
    plot(t3,y3);
    plot(t1,r1,'--');
    grid
    s1=sprintf("p=%.2f",p1);
    s2=sprintf("p=%.2f",p2);
    s3=sprintf("p=%.2f",p3);
    legend(s1,s2,s3);
    xlim([0,2.5]);
    saveas(fig,'comparaciones/seg_y.fig');
    
    fig=figure;hold on;
    plot(t1,u1);
    plot(t2,u2);
    plot(t3,u3);
    grid;
    xlim([0,2.5]);
    saveas(fig,'comparaciones/seg_u.fig');
      
    
    
    [y1,u1,t1] = sim_reg(Kp1,Ki1,Kd1,tsim);
    [y2,u2,t2] = sim_reg(Kp2,Ki2,Kd2,tsim);
    [y3,u3,t3] = sim_reg(Kp3,Ki3,Kd3,tsim);

    fig=figure;hold on;
    plot(t1,y1);
    plot(t2,y2);
    plot(t3,y3);
    grid;
    xlim([0,5]);
    saveas(fig,'comparaciones/reg_y.fig');
    
    fig=figure;hold on;
    plot(t1,u1);
    plot(t2,u2);
    plot(t3,u3);
    grid;
    xlim([0,5]);
    saveas(fig,'comparaciones/reg_u.fig');
    close all
end

