function J = J_reg(u,y,Q,R,u_max)
    % Función que calcula el coste para el seguimiento
    % Entradas:
    % - u: vector de entradas
    % - y: vector de salidas
    % - Q: peso del error
    % - R: peso de la señal de control
    % - u_max: señal de control máxima perminida
    
    e=y;
    sum_e=trapz(e.^2);
    sum_u=trapz(u.^2);
    u_max_act=max(abs(u));
    J=R*sum_u + Q*sum_e;
    if u_max_act>=u_max 
        J=J+1e6*(u_max_act-u_max);
    end
end

