   function int = Coste(r,y,t)
 % Función que calcula el coste
    % Entradas:
    % - r: vector de referencias 
    % - y: vector de salidas
    % - t: vector de tiempos
    
    e=abs(r-y); % Se calcula el error valor absoluto del error
    int=0; % integral en el seguimiento
    for i=2:length(t)
            int=int+0.5*(e(i)+e(i-1)); % integral trapezoidal
    end
end

