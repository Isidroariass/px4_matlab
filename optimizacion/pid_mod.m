Jx=0.03;
tau_m=0.15;


damping=1;
wn=1/tau_m*2;
Kp=wn^2*Jx*tau_m
Kd=damping*2*wn*Jx*tau_m-Jx



N=Kp;
D=[Jx*tau_m, Jx+Kd, Kp]
G=tf(N,D);

ts=pi/(wn)
%pzmap(G)
%%
clear
syms s Jx tau_m Kp Kd real

G=1/(Jx*s*(tau_m*s+1));
G_bc=Kp*G/(1+Kp*G+Kd*G*s);
G_bc=simplify(G_bc);
pretty(G_bc)

