function out = fun_op(x)
    Kp=x(1)
    Ki=x(2)
    Kd=x(3)
    establece_ganancias(Kp,Ki,Kd); % Función que modifica las ganancias del controlador que se esté optimizando (el de velocidad angular, orientación...) y del eje que se está probando (x, y o z)
    datos = sim_sk(); % Se ejecuta la simulación y se guarda los resultados en el objeto 'datos'
    [y,r,t] = obtiene_resultados(datos); % Se extraen la medida (y), la referencia (r) y de tiempo (t) del objeto 'datos'.
    J=Coste(r,y,t); % Se calcula el coste
    out=J;
end
