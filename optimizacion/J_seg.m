   function J = J_seg(r,y,t)
 % Función que calcula el coste para el seguimiento
    % Entradas:
    % - r: vector de referencias 
    % - y: vector de salidas
    % - t: vector de tiempos
    global param
    
    e=abs(r-y); % Se calcula el error valor absoluto del error
    int=0; % integral en el seguimiento
    int_pert=0; % integral en el rechazo a perturbaciones
    for i=2:length(t)
        if t(i)<param.delay_pert
            int=int+0.5*(e(i)+e(i-1))^1*(t(i)-t(i-1))*t(i)^0;
        else
            int_pert=int_pert+0.5*(e(i)+e(i-1))^1*(t(i)-t(i-1))*(t(i)-param.delay_pert)^0; % En el rechazo a perturbaciones se desplaza el eje temporal de manera sea t=0 en el momento de la perturbación
        end
    end
    [int, int_pert];
    J=int+int_pert*param.peso_pert
end

