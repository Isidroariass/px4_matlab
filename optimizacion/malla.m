clear all
parametros;
kp=2.03220126375641;
ki=0.22905946725769;
kd=0.00628513784600225;
i=1;j=1;
np=15;
ni=15;
nd=15;
limp=0.185;
lowp=0.189;
limi=0.229;
lowi=0.5;
limd=0.006;
lowd=0.01;

%% Ki, Kd
% for kd=lowd:(limd-lowd)/nd:limd
%     for ki=lowi:(limi-lowi)/ni:limi
%         ki_vec(j)=ki;
%         resultados(i,j)=fun_op([kp,kd,ki]);
%         j=j+1;
%     end
%     kd_vec(i)=kd;
%     i=i+1
%     j=1;
% end  

for kd=lowd:(limd-lowd)/nd:limd
    for ki=lowi:(limi-lowi)/ni:limi
        ki_vec(j)=ki;
        resultados(i,j)=fun_op([kp,kd,ki]);
        j=j+1;
    end
    kd_vec(i)=kd;
    i=i+1
    j=1;
end  

mesh(resultados);