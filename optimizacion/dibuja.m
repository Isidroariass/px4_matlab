function dibuja(x)

    Kp=x(1);
    Kd=x(2);
    Ki=x(3);
    min_step=0.5e-3;

    global Jx
    global tau_m
    Jx=0.03;
    tau_m=0.15;
    Tm=1e-3;
    tsim=20;
    
    in = Simulink.SimulationInput('pid_com2');
    in = in.setVariable('Jx',Jx);
    in = in.setVariable('Kp',Kp);
    in = in.setVariable('Kd',Kd);
    in = in.setVariable('Ki',Ki);
    in = in.setVariable('Kff',0);
    in = in.setVariable('pert',0);
    
    in = in.setVariable('Jx',Jx);
    in = in.setVariable('tau_m',tau_m);
    in = in.setVariable('tsim',tsim);
    in = in.setVariable('Tm',Tm);
    in = in.setVariable('min_step',min_step);
    in=in.setModelParameter('SaveOutput','on');
    in=in.setModelParameter('SaveState','on');
    
    out=sim(in);
    y=out.y_out(1,1).data;
    u=out.u_out(1,1).data;    
    r=out.r_out(1,1).data;
    t=out.t(1,1).data;

    fig=figure;
    plot(t,y,t,r);
    saveas(fig,'figuras/seg_y.fig');
    
    fig=figure;
    plot(t,u);
    saveas(fig,'figuras/seg_u.fig');
        
    in = in.setVariable('pert',1);
    out=sim(in);
    y=out.y_out(1,1).data;
    u=out.u_out(1,1).data; 
    t=out.t(1,1).data;

    fig=figure;
    plot(t,y);
    saveas(fig,'figuras/reg_y.fig');
    
    fig=figure;
    plot(t,u);
    saveas(fig,'figuras/reg_u.fig');
    close all
end

