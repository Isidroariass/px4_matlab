function out = sim_sk()
    global param 

    in = Simulink.SimulationInput('tiltrotor_sk');
    in = in.setVariable('param',param);
    in=in.setModelParameter('SaveOutput','on');
    in=in.setModelParameter('SaveState','on');

    out=sim(in);
    

end
