%% Diseño simbolico
clear all
syms s Jx tau_m Kp Kd Ki real

G=1/(Jx*s*(tau_m*s+1));
C=Kp+Ki/s;
% C=Kp;
G_bc=C*G/(1+C*G+Kd*G*s);
G_bc=simplify(G_bc);
pretty(G_bc)

G_bc_pert=G/(1+C*G+Kd*G*s);
G_bc_pert=simplify(G_bc_pert);
pretty(G_bc_pert)

%%
clear all
Jx=0.03;
tau_m=0.15;
Kp=0.8;Kd=0.09;Ki=1;
N=[Kp Ki];
D=[Jx*tau_m, Jx+Kd, Kp,Ki];
G=tf(N,D);
step(G,1)

N_p=[1 0];
D_p=D;
G_p=tf(N_p,D_p);
%step(G_p,5);



