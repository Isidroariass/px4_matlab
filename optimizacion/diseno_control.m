clear all;close all;clc;
%% Diseño del controlador de estabilización
Jx=0.03;     % kg.m²
Jy=0.04;     % kg.m²
m=1;      % kg

% Modelo rotacional aproximado
% Tau_phi=Jx*phi_d
% Tau_theta=Jx*theta_d
tau_m=0.05;
G_m=tf(1,[tau_m 1])
G_roll=tf(1/Jx,[1 0])
G_roll=G_roll*G_m

t_s_BC=0.3;     % s
tau_BC=t_s_BC/3;
p_BC=-1/tau_BC;

%C_roll=tf(kp*[tau_c, 1],1);
C_roll=1;
 rltool(G_roll,C_roll);
 
 

 
return


%% Controlador de la posicion x
m=1.4;
g=9.8;
T_hov=m*g;
p_roll=10;
c_roll=5;
D=conv([1 p_roll],[1 p_roll]);
D=conv([1 0 0],D);
N=T_hov/m;
N=N*[1 c_roll]
G_bc_roll=tf(N,D)
Kd=1/9.8;Kp=0;Ki=0;
Cx=tf([Kd Kp Ki],[1 0])
rltool(G_bc_roll,Cx)
return


Kp=1;
Td=1;
Ti=10;
roots([Ti*Td, Ti, 1])

Cx=tf(Kp*[Ti*Td, Ti, 1], [Ti 0]);
cero=11
Cx=tf(4.8*conv([1 cero],[1 cero]),[1 0])
rltool(G_roll_bc,Cx)
return


kp=Jz*4/Td^2;
Cz=tf(kp*[Td, 1],1);    
%s = sisoinit(1);
%s.G.value=Gx;
%s.C.value=Cx;
%s.OL1.Name = 'Control de estabilización del eje x';
%s.OL1.View = {'rlocus'};
%controlSystemDesigner(s);
kd=Td*kp;


display('scrip disenio_control terminado');
%exit;
