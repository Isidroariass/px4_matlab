function establece_ganancias(Kp,Ki,Kd)
% establece_ganancias: Función que establece las ganacias del controlador indicado

    % Permitir solo ganancias positivas
    Kp=abs(Kp);
    Ki=abs(Ki);
    Kd=abs(Kd);

    global param 
    
    switch param.modoControl
        case 2 % Control de velocidad angular 
            switch param.eje_exp 
                case 0
                    param.MC_ROLLRATE_P=Kp;
                    param.MC_ROLLRATE_I=Ki;
                    param.MC_ROLLRATE_D=Kd;
                case 1
                    param.MC_PITCHRATE_P=Kp;
                    param.MC_PITCHRATE_I=Ki;
                    param.MC_PITCHRATE_D=Kd;
                case 2
                    param.MC_YAWRATE_P=Kp;
                    param.MC_YAWRATE_I=Ki;
                    param.MC_YAWRATE_D=Kd;
                otherwise
                    error('valor de eje_exp erróneo')
            end
            
        case 3 % Control de orientacion
            switch param.eje_exp 
                case 0
                    param.MC_ROLL_P=Kp;
                case 1
                    param.MC_PITCH_P=Kp;
                case 2
                    error('no implementado todavía')
                otherwise
                    error('valor de eje_exp erróneo')
            end
            
        case 5 % Control de velocidad
            switch param.eje_exp 
                case 0
                    param.MPC_XY_VEL_P= Kp;
                    param.MPC_XY_VEL_I= Ki;
                    param.MPC_XY_VEL_D= Kd;
                case 1 % El controlador en el eje y es el mismo que en el x
                    param.MPC_XY_VEL_P= Kp;
                    param.MPC_XY_VEL_I= Ki;
                    param.MPC_XY_VEL_D= Kd;
                case 2
                    param.MPC_Z_VEL_P=Kp;
                    param.MPC_Z_VEL_I=Ki;
                    param.MPC_Z_VEL_D=Kd;
                otherwise
                    error('valor de eje_exp erróneo')
            end

        case 6 % Controlador de posición
            switch param.eje_exp 
                case 0
                    param.MPC_XY_P=Kp;
                case 1
                    param.MPC_XY_P=Kp;
                case 2
                    param.MPC_Z_P=Kp;
                otherwise
                    error('valor de eje_exp erróneo')
            end
        otherwise
            error('valor de modo_control no valido para la optimizacion')
    end
end

