function [y,r,t] = obtiene_resultados(out)
    global param
    
    switch param.modoControl
        case 2 % Control de velocidad angular 
            switch param.eje_exp 
                case 0
                    y=out.omega.Data(:,1);  
                    r=out.omega_sp.Data(:,1);
                    t=out.omega.Time;
                case 1
                    y=out.omega.Data(:,2);  
                    r=out.omega_sp.Data(:,2);
                    t=out.omega.Time;
                case 2
                    y=out.omega.Data(:,3);  
                    r=out.omega_sp.Data(:,3);
                    t=out.omega.Time;
                otherwise
                    error('valor de eje_exp erróneo')
            end
            
        case 3 % Control de orientacion
            switch param.eje_exp 
                case 0
                    y=out.euler.Data(:,1);  
                    r=param.eul_ref(1);
                    t=out.euler.Time;
                case 1
                    y=out.euler.Data(:,2);  
                    r=param.eul_ref(2);
                    t=out.euler.Time;
                case 2
                    y=out.euler.Data(:,3);  
                    r=param.eul_ref(3);
                    t=out.euler.Time;
                otherwise
                    error('valor de eje_exp erróneo')
            end
            
        case 5 % Control de velocidad
            switch param.eje_exp 
                case 0 % Si se evalua la rotación en el eje x, habrá movimiento en el eje y
                    y=out.Xd.Data(:,2);  
                    r=out.Xd_sp.Data(:,2);
                    t=out.Xd.Time;
                case 1 
                    y=out.Xd.Data(:,1);  
                    r=out.Xd_sp.Data(:,1);
                    t=out.Xd.Time;
                case 2
                    y=out.Xd.Data(:,3);  
                    r=out.Xd_sp.Data(:,3);
                    t=out.Xd.Time;
                otherwise
                    error('valor de eje_exp erróneo')
            end

        case 6 % Controlador de posición
            switch param.eje_exp 
                case 0
                    y=out.X.Data(:,2);  
                    r=out.X_sp.Data(:,2);
                    t=out.X.Time;
                case 1 
                    y=out.X.Data(:,1);  
                    r=out.X_sp.Data(:,1);
                    t=out.X.Time;
                case 2
                    y=out.X.Data(:,3);  
                    r=out.X_sp.Data(:,3);
                    t=out.X.Time;
                otherwise
                    error('valor de eje_exp erróneo')
            end
        otherwise
            error('valor de modo_control no valido para la optimizacion')
    end

end

