function out= mc_att_control(in)
   q_sp= [in(1);in(2);in(3);in(4)]'; % Orientación de referencia
   q=    [in(5);in(6);in(7);in(8)]'; % Orientación actual

   % Simulink inicializa los cuaternios a 0, algo que puede dar problemas
   if norm(q)==0
       q=q_sp;
   end

   global param
   
   Kp=[param.MC_ROLL_P;param.MC_PITCH_P;(param.MC_ROLL_P+param.MC_PITCH_P)/2];

   % Verison simplificada de la ley de control angular de PX4. Cuando solo
   % se controla un eje no deberia de haber diferencias.
   qe=quatmultiply(quatinv(q),q_sp)';
   omega_sp=2*Kp.*sign(qe(1)).*qe(2:4);
   
   limites=[param.MC_ROLLRATE_MAX;param.MC_PITCHRATE_MAX;param.MC_YAWRATE_MAX]*pi/180;
   omega_sp=min(omega_sp, limites);
   omega_sp=max(omega_sp,-limites);
   
   out=omega_sp;    
end
