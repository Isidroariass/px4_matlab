Omega=100;
D = 0.6043;       % Diámetro de la hélice del propulsor [m].
nEngines = 2;     % Número de motores.

CT0 =  0.0891;
rho=1.2133;

% Formulado sin que aparezca el parámetro de avance explícitamente. Es más conveniente para poder
% analizar el caso de Omega = 0 y/o Va=0 sin que dé errores de división por cero o
% indeterminaciones.
Thrust = nEngines*rho*D^2*CT0*D^2 * Omega^2
Thrust = nEngines*rho*D^4*CT0/(2*pi)^2*(Omega*2*pi)^2
ct=rho*D^4*CT0/(2*pi)^2;
Thrust = 2*ct*(Omega*2*pi)^2