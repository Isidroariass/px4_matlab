function out=mc_att_rate_control(in)
   rate_sp_y=[in(1);in(2); in(3)];
   rate_y=[in(4);in(5); in(6)];
   e_int=[in(7);in(8); in(9)];
   rate_d=[in(10);in(11); in(12)];

   global param
   kp=[param.MC_ROLLRATE_P;param.MC_PITCHRATE_P;param.MC_YAWRATE_P];
   ki=[param.MC_ROLLRATE_I;param.MC_PITCHRATE_I;param.MC_YAWRATE_I];
   kd=[param.MC_ROLLRATE_D;param.MC_PITCHRATE_D;param.MC_YAWRATE_D];
   tau= kp.*( rate_sp_y - rate_y) + ...
      kd.*( 0 - rate_d) + ki.*e_int;
   
   tau(1)=min(  param.max_taux, tau(1));
   tau(1)=max(-param.max_taux, tau(1));
   tau(2)=min(  param.max_tauy, tau(2));
   tau(2)=max(-param.max_tauy, tau(2));
   tau(3)=min(  param.max_tauz, tau(3));
   tau(3)=max(-param.max_tauz, tau(3));
   out=tau;
end

    
