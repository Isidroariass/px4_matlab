function out = genera_tau_f(in)

   omega1=in(1);
   omega2=in(2);
   alfa1=in(3);
   alfa2=in(4);

   global param
   ly=param.ly;
   lz=param.lz;

   t1=omega1*omega1*param.cT;
   t2=omega2*omega2*param.cT;
   T1=[-t1*sin(alfa1);   0; -t1*cos(alfa1)];
   T2=[-t2*sin(alfa2);   0; -t2*cos(alfa2)];
   taud1=omega1*omega1*param.cm;
   taud2=omega2*omega2*param.cm;

tau=[...
   ly*t2*cos(alfa2)-ly*t1*cos(alfa1)-taud1*sin(alfa1)+taud2*sin(alfa2);
   lz*t1*sin(alfa1)+lz*t2*sin(alfa2)                                  ;
   ly*t1*sin(alfa1)-ly*t2*sin(alfa2)-taud1*cos(alfa1)+taud2*cos(alfa2)];

out=[tau;T1+T2];
    if alfa1~=0

    end
end
