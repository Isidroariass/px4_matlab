clc;clear;close all
B = [
 -0.500000, -0.000000,  5.000000,  0.250000;
  0.500000, -0.000000,  5.000000,  0.25;
 -0.000000,  0.500000, -5.000000,  0.250000;
 -0.000000, -0.500000, -5.000000,  0.250000];
A=inv(B);
% B = [
%  -0.500000,  0.500000,  5.000000,  4;
%   0.500000, -0.500000,  5.000000,  4;
%  -0.500000,  0.500000, -5.000000,  4;
%   0.500000, -0.500000, -5.000000,  4];
F=[5;2;1;8];
F_no_yaw=F;
F_no_yaw(3)=0;
out=B*F_no_yaw


% THRUST
k=calcula_desat(out,B(:,4));
out_sat1=out+k*B(:,4)
k=calcula_desat(out_sat1,B(:,4))/2;
out_sat2=out_sat1+k*B(:,4)


% ROLL

k=calcula_desat(out_sat2,B(:,1));
out_sat3=out_sat2+k*B(:,1)
k=calcula_desat(out_sat3,B(:,1))/2;
out_sat4=out_sat3+k*B(:,1)


% PITCH

k=calcula_desat(out_sat4,B(:,2));
out_sat5=out_sat4+k*B(:,2)
k=calcula_desat(out_sat5,B(:,2))/2;
out_sat6=out_sat5+k*B(:,2)


F_only_yaw=[0;0;F(3);0];

out_sat7=out_sat6 + B*F_only_yaw;

k=calcula_desat(out_sat7,B(:,3));
out_sat8=out_sat7+k*B(:,3);

k=calcula_desat(out_sat8,B(:,4));
out_sat9=out_sat8+k*B(:,1);


F1=A*out_sat1;
F2=A*out_sat2;
F3=A*out_sat3;
F4=A*out_sat4;
F5=A*out_sat5;
F6=A*out_sat6;


%% graficas
figure

dibuja(out);

figure;
subplot(3,2,1)
dibuja(out_sat1);
subplot(3,2,2)
dibuja(out_sat2);
subplot(3,2,3)
dibuja(out_sat3);
subplot(3,2,4)
dibuja(out_sat4);
subplot(3,2,5)
dibuja(out_sat5);
subplot(3,2,6)
dibuja(out_sat6);

figure;
subplot(2,1,1)
dibuja(out_sat7)
subplot(2,1,2)
dibuja(out_sat8)


