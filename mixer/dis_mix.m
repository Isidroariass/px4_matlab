syms lz alpha1 alpha2 t1 t2  ly taud1 taud2 real
% alpha1=alpha2;t1=t2;
T1=[-t1*sin(alpha1), 0, -t1*cos(alpha1)]';
T2=[-t2*sin(alpha2), 0, -t2*cos(alpha2)]';
P1=[0;ly;-lz];
P2=[0;-ly;-lz];
Taud1=[-taud1*sin(alpha1), 0, -taud1*cos(alpha1)]';
Taud2=[taud2*sin(alpha2), 0, taud2*cos(alpha2)]';

tau1=cross(P1,T1);
tau2=cross(P2,T2);
tau=tau1+tau2+Taud1+Taud2
Ft=T1+T2;



