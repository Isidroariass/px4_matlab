function k = calcula_desat(out,Bi)

kmax=0;
kmin=0;
for i=1:4
    if abs(Bi(i))< 0.000001
        continue
    end
    
    if out(i)>1
        k=(1-out(i))/Bi(i);
        if k<kmin
            kmin=k;
        end
        if k>kmax
            kmax=k;
        end
    end
   
   if out(i)<0
       k=(0-out(i))/Bi(i);
        if k<kmin
            kmin=k;
        end
        if k>kmax
            kmax=k;
        end
    end
end
k=kmax+kmin;
end

