close all;clear all;
datos=load('prueba_fichero.txt');
t=datos(:,1);
RelativeAngularVel_x=datos(:,2);
RelativeAngularVel_y=datos(:,3);
RelativeAngularVel_z=datos(:,4);
RelativeForce_x=datos(:,5);
RelativeForce_y=datos(:,6);
RelativeForce_z=datos(:,7);
RelativeLinearAccel_x=datos(:,8);
RelativeLinearAccel_y=datos(:,9);
RelativeLinearAccel_z=datos(:,10);

RelativeLinearVel_x=datos(:,11);
RelativeLinearVel_y=datos(:,12);
RelativeLinearVel_z=datos(:,13);

vel_joint_0_x=datos(:,14);
vel_joint_0_y=datos(:,15);
vel_joint_0_z=datos(:,16);
ang_joint_0_x=datos(:,17);
ang_joint_0_y=datos(:,18);
ang_joint_0_z=datos(:,19);

LinkForce_x=datos(:,20);
LinkForce_y=datos(:,21);
LinkForce_z=datos(:,22);


LinkTorque_x=datos(:,23);
LinkTorque_y=datos(:,24);
LinkTorque_z=datos(:,25);


figure;
plot(t,[RelativeAngularVel_x RelativeAngularVel_y RelativeAngularVel_z]);
legend('x','y','z');
title('RelativeAngularVel');

figure;
plot(t,[RelativeForce_x RelativeForce_y RelativeForce_z]);
legend('x','y','z');
title('RelativeForce');

figure;
plot(t,[RelativeLinearAccel_x RelativeLinearAccel_y RelativeLinearAccel_z]);
legend('x','y','z');
title('RelativeLinearAccel');

figure;
plot(t,[RelativeLinearVel_x RelativeLinearVel_y RelativeLinearVel_z]);
legend('x','y','z');
title('RelativeLinearVel');

figure;
plot(t,[vel_joint_0_x vel_joint_0_y vel_joint_0_z]);
title('Velocidad motor_joint_0')

figure;
plot(t,[ang_joint_0_x ang_joint_0_y ang_joint_0_z]);
title('Angulo motor_joint_0')

figure;
plot(t,[LinkForce_x LinkForce_y LinkForce_z]);
legend('x','y','z');
title('Fuerza');

figure;
plot(t,[LinkTorque_x LinkTorque_y LinkTorque_z]);
legend('x','y','z');
title('Par');