function out=forces2Angle(in)
   Fx=in(1);
   Fz=in(2);
  
   % Para avanzar en el ejex es necesarion pich<0
   pitch=-atan2(Fx,-Fz);
   
   thr_body=Fz/cos(pitch); %En ejes body NED
   out=[pitch;thr_body];
end
